package conanTheBarbarian;

import java.util.Random;

public class ConanTheBarbarian extends Fighter {

	private static ConanTheBarbarian conan;

	private ConanTheBarbarian(String name, int ko) {
		super(name, ko);
	}

	public static ConanTheBarbarian challangeConan() {
		if (conan == null) {
			conan = new ConanTheBarbarian("Conan", 10);
		}
		return conan;
	}

	@Override
	public String getStats() {
		return "" + 50;
	}

	@Override
	public boolean hit() {
		return new Random().nextBoolean();
	}
}
