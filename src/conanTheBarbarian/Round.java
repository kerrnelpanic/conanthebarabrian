package conanTheBarbarian;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Round {
	private static Random random = new Random();

	public static List<Fighter> determineWinners(List<Fighter> fighters) {
		List<Fighter> winners = new ArrayList<>();

		Fighter fighter1;
		Fighter fighter2;
		int fights = fighters.size() / 2;
		for (int i = 0; i < fights; i++) {
			fighter1 = fighters.remove(random.nextInt(fighters.size()));
			fighter2 = fighters.remove(random.nextInt(fighters.size()));
			HoldTournament.clearScreen();
			winners.add(letFight(fighter1, fighter2));
		}
		winners.addAll(fighters);
		return winners;
	}

	public static Fighter letFight(Fighter fighter1, Fighter fighter2) {
		System.out.println("Fight: " + fighter1 + " vs " + fighter2);
		System.out.println("Stats: " + fighter1.getStats() + " vs " + fighter2.getStats());
		try {
			Thread.sleep(HoldTournament.gameSpeed * 20);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Fighter winner = null;

		while (winner == null) {
			if (random.nextBoolean()) {
				winner = hasWon(fighter1, fighter2);
			} else {
				winner = hasWon(fighter2, fighter1);
			}
		}
		return winner;
	}

	public static Fighter hasWon(Fighter attacker, Fighter defender) {

		if (attacker.hit()) {
			System.out.println(attacker + " beheads " + defender);
			try {
				Thread.sleep(HoldTournament.gameSpeed * 15);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (!(attacker instanceof ConanTheBarbarian || defender instanceof ConanTheBarbarian))
				knockOutAnimation();
			return attacker;
		}
		System.out.println(attacker + " misses " + defender);
		try {
			Thread.sleep(HoldTournament.gameSpeed * 5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void knockOutAnimation() {
		int animationSpeed = HoldTournament.gameSpeed * 2;
		try {

			HoldTournament.clearScreen();
			System.out.println("o" + "   " + "o");
			System.out.println("|-;" + " " + "|-");
			System.out.println("^" + "   " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println("o" + "   " + "o");
			System.out.println("|--;" + "" + "|-");
			System.out.println("^" + "   " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println("o" + "   " + " o");
			System.out.println("|--;" + "" + " |-");
			System.out.println("^" + "   " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println("o" + "   " + "");
			System.out.println("|--;" + "" + " |- o");
			System.out.println("^" + "   " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println("o" + "   " + "");
			System.out.println("|--;" + "" + "");
			System.out.println("^" + "   " + "^ /- o");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println("o" + "   " + "");
			System.out.println("|--;" + "" + "");
			System.out.println("^" + "   " + "^ /-  o");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println("o" + "   " + "");
			System.out.println("|--;" + "" + "");
			System.out.println("^" + "   " + "^ /-   o");
			Thread.sleep(HoldTournament.gameSpeed * 5);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void conanDies() {
		int animationSpeed = HoldTournament.gameSpeed * 2;
		try {

			HoldTournament.clearScreen();
			System.out.println(" o _/");
			System.out.println(" |" + "     " + "o");
			System.out.println(" ^" + "     " + "|-");
			System.out.println("/ \\" + "    " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o __/");
			System.out.println(" |" + "      " + "o");
			System.out.println(" ^" + "      " + "|-");
			System.out.println("/ \\" + "     " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o");
			System.out.println(" | __/" + "  " + "o");
			System.out.println(" ^" + "      " + "|-");
			System.out.println("/ \\" + "     " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o");
			System.out.println(" | ___" + "  " + "o");
			System.out.println(" ^ " + "     " + "|-");
			System.out.println("/ \\" + "     " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o __");
			System.out.println(" |   \\" + "  " + "o");
			System.out.println(" ^" + "      " + "|-");
			System.out.println("/ \\" + "     " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o _");
			System.out.println(" | _\\" + "   " + "o");
			System.out.println(" ^" + "      " + "|-");
			System.out.println("/ \\" + "     " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o _");
			System.out.println(" |_/" + "    " + "o");
			System.out.println(" ^" + "      " + "|-");
			System.out.println("/ \\" + "     " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o");
			System.out.println("|__\\" + "    " + "o");
			System.out.println(" ^" + "      " + "|-");
			System.out.println("/ \\" + "     " + "^");
			Thread.sleep(animationSpeed * 2);

			HoldTournament.clearScreen();
			System.out.println(" o   " + "   " + "o");
			System.out.println("|__\\" + "    " + "|-");
			System.out.println(" ^ " + "     " + "^");
			Thread.sleep(animationSpeed * 2);

			HoldTournament.clearScreen();
			System.out.println("        " + "o");
			System.out.println(" o" + "      " + "|-");
			System.out.println("|__\\" + "    " + "^");
			Thread.sleep(animationSpeed * 2);

			HoldTournament.clearScreen();
			System.out.println("        " + "o");
			System.out.println("        " + "|-");
			System.out.println(" o" + "      " + "^");
			Thread.sleep(animationSpeed * 2);

			HoldTournament.clearScreen();
			System.out.println("        " + "o");
			System.out.println("        " + "|-");
			System.out.println(" c;" + "     " + "^");
			Thread.sleep(animationSpeed * 2);

			HoldTournament.clearScreen();
			System.out.println("        " + "o");
			System.out.println("        " + "|-");
			System.out.println(" c,." + "    " + "^");
			Thread.sleep(animationSpeed * 5);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void conanIsInvincibleAnimation() {
		int animationSpeed = HoldTournament.gameSpeed * 2;
		try {

			HoldTournament.clearScreen();
			System.out.println(" o _/");
			System.out.println(" |  " + "   " + "o");
			System.out.println(" ^ " + "    " + "|-");
			System.out.println("/ \\" + "    " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o __/");
			System.out.println(" |  " + "   " + "o");
			System.out.println(" ^ " + "    " + "|-");
			System.out.println("/ \\" + "    " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o");
			System.out.println(" | __/" + " " + "c ,");
			System.out.println(" ^ " + "    " + "|-");
			System.out.println("/ \\" + "    " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o");
			System.out.println(" | ___" + " " + "");
			System.out.println(" ^ " + "    " + "`, c ,");
			System.out.println("/ \\" + "    " + "^");
			Thread.sleep(animationSpeed);

			HoldTournament.clearScreen();
			System.out.println(" o");
			System.out.println(" | __/" + " " + "");
			System.out.println(" ^ " + "    " + "");
			System.out.println("/ \\" + "    " + "^ , c ,");
			Thread.sleep(animationSpeed * 5);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
