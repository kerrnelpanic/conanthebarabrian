package conanTheBarbarian;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class HoldTournament {

	static int gameSpeed = 100;// 100 for normal speed
	static Scanner scannerIn = new Scanner(System.in);

	public static void main(String[] args) {
		if (args.length > 0) {
			if (args[0].equals("-h")) {
				System.out.println("Usage: java HoldTournament [options]");
				System.out.println("\t-s <number>\tgame speed \ta number between 1 and 200 to run the game smoothly");
				System.out.println("\t-h\t\thelp\t\tthis help menu");
				return;
			}
			if (args[0].equals("-s")) {
				gameSpeed = Integer.parseInt(args[1]);
			}
		}

		Arena arena1 = initArena();
		populateArena(arena1);

		clearScreen();
		System.out.println("Arena Round 1");
		Arena arena2 = arena1.letFight();

		clearScreen();
		System.out.println("Arena Round 2");
		Arena arena3 = arena2.letFight();

		clearScreen();
		System.out.println("Arena Round 3");
		Arena arena4 = arena3.letFight();

		clearScreen();
		System.out.println("Arena Endgame");
		arena4.letFight();

	}

	private static Arena initArena() {
		System.out.println("Where will the fighting take place?\nYou may leave this field empty");
		String name = scannerIn.nextLine();

		if (name.length() < 1)
			name = "Deadly Graveyard of Darkness of Doom";

		System.out.println("Your new Arena: " + name);

		return new Arena(name);
	}

	private static void populateArena(Arena arena) {
		System.out.println("You can create up to 8 fighters");
		for (int i = 0; i < 8; ++i) {
			System.out.println("Do You want to add a fighter? (y/n)");
			if (scannerIn.nextLine().equals("y"))
				newFighterView(arena);
			else
				break;
		}

		Random random = new Random();
		ArrayList<Fighter> fighters = new ArrayList<Fighter>();
		fighters.add(new Fighter("Ada", 8));
		fighters.add(new Fighter("Donald", 5));
		fighters.add(new Fighter("Konrad", 7));
		fighters.add(new Fighter("Alan", 8));
		fighters.add(new Fighter("Margaret", 10));
		fighters.add(new Fighter("Edsger", 9));
		fighters.add(new Fighter("Linus", 7));
		fighters.add(new Fighter("Richard", 6));
		fighters.add(new Fighter("Ken", 6));
		fighters.add(new Fighter("Niklaus", 6));

		while (arena.getFighters().size() < 8) {
			arena.addFighter(fighters.remove(random.nextInt(fighters.size())));
		}
	}

	static void confirmView(String message) {
		System.out.println(message);
		System.out.println("Any key to continue");
		scannerIn.nextLine();
	}

	public static void clearScreen() {
		for (int i = 0; i < 20; i++) {
			System.out.print("\n\n\n\n\n\n\n\n\n\n");
		}
	}

	private static void newFighterView(Arena arena) {
		System.out.println("Name Your fighter");
		String name = scannerIn.nextLine();
		System.out.println("Choose 5-10 as Your fighter's ko stat");
		int ko = scannerIn.nextInt();
		scannerIn.nextLine();
		Fighter f = new Fighter(name, ko);
		System.out.println("Your fighters name is " + f.getName() + " and their ko-ability is " + f.getStats());
		System.out.println("Do You want to add this fighter to the Arena? (y/n)");
		if (scannerIn.nextLine().equals("y"))
			arena.addFighter(f);
	}
}
