package conanTheBarbarian;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Arena {

	static Scanner scannerIn = new Scanner(System.in);

	private class Fight {
		Fighter fighter1;
		Fighter fighter2;

		private Fight(Fighter fighter1, Fighter fighter2) {
			this.fighter1 = fighter1;
			this.fighter2 = fighter2;
		}
	}

	private String name;
	private List<Fighter> fighters;
	private List<Fight> fights;
	private StringBuilder history;

	public Arena(String name) {
		this.name = name;
		this.fighters = new ArrayList<>();
		this.fights = new ArrayList<>();
		this.history = new StringBuilder();
	}

	private Arena(Arena source) {
		this.name = source.name;
		this.fighters = new ArrayList<Fighter>(source.fighters);
		this.fights = new ArrayList<>();
		this.history = new StringBuilder();
	}

	public ArrayList<Fighter> getFighters() {
		return new ArrayList<>(fighters);
	}

	public ArrayList<Fighter> addFighter(Fighter fighter) {
		fighters.add(fighter);
		return new ArrayList<Fighter>(fighters);
	}

	public Arena letFight() {
		if (fighters.size() < 2) {
			fightConan(fighters.get(0));
			return null;
		}

		if (fights.size() < 1) {
			diceFights();
			scribeRound();
		}
		System.out.println(this);
		HoldTournament.confirmView("");

		fighters = Round.determineWinners(fighters);

		HoldTournament.clearScreen();
		HoldTournament.confirmView(roundResult());

		return new Arena(this);
	}

	@Override
	public String toString() {
		return history.toString();
	}

	public String roundResult() {
		StringBuilder sb = new StringBuilder("The survivors: ");

		fighters.forEach(f -> sb.append(f + ", "));
		sb.delete(sb.length() - 2, sb.length());

		return sb.toString();
	}

	private void scribeRound() {
		history.append("In " + name + " are " + fighters.size() + " fighters: ");
		fighters.forEach(f -> history.append(f.toString() + ", "));
		history.delete(history.length() - 2, history.length());
		history.append(System.lineSeparator());
		history.append("The Fights: ");
		fights.forEach(f -> history.append(f.fighter1 + " vs " + f.fighter2 + ", "));
		history.delete(history.length() - 2, history.length());
		history.append(System.lineSeparator());
	}

	private void diceFights() {
		ArrayList<Fighter> tuple = new ArrayList<>(fighters);
		Random random = new Random();
		while (tuple.size() > 1) {
			fights.add(
					new Fight(tuple.remove(random.nextInt(tuple.size())),
							tuple.remove(random.nextInt(tuple.size()))));
		}
	}

	public void fightConan(Fighter challanger) {
		System.out.println("Do You want to die? (y/n)");
		String choice = scannerIn.next();
		if (choice.equals("y")) {
			if (Round.letFight(challanger, ConanTheBarbarian.challangeConan()) instanceof ConanTheBarbarian) {
				Round.conanIsInvincibleAnimation();
				System.out.println("You're dead");
			} else {
				Round.conanDies();
				System.out.println("Man, you've killed my singleton");
			}
		}

	}
}
