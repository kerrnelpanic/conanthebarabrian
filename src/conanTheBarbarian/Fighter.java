package conanTheBarbarian;

import java.util.Random;

public class Fighter {
	private final String name;
	private final int ko;

	public Fighter(String name, int ko) {
		this.name = name;
		int valid = ko;
		if (ko < 5)
			valid = 5;
		if (ko > 10)
			valid = 10;
		this.ko = valid;
	}

	public String getName() {
		return name;
	}

	public String getStats() {
		return "" + ko;
	}

	public boolean hit() {
		if (new Random().nextInt(99) + 1 <= ko)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return name;
	}

}
