# README #
This is a tournament simulation. A tournament has 8 contesters, fighting each other to death. The survivor is eligible to contest the deadly, brutal, violent, fierce, invincible, ruthless and, not to forget, absolutely deadly end boss, the deadly **CONAN the Barbarian** (did I mention that he is deadly?).  
Disclaimer: this console animation game shows deadly violence in great numbers.  
Version 1.0.0  
Testet with LibericaJDK-8-Full
### setup ###
Entry point can be found in *HoldTournament.java*.  

Clone the repository to a suitable directory on Your System (eg. /home/YourUserHome/conan). You can copy-paste the command from the gitbucket-page, just click on 'clone', copy the line and paste it to your console in the prepared directory.  

If You're really lazy and trust in me, You can just execute **bash Conan.bash** in the new folder (On Windows systems use a git Bash).  

Otherwise, move into the new conanthebarbarian directory and execute the comand ** javac -d bin src/conanTheBarbarian/* ** to compile the classfiles.  
Run the game with **java -cp bin/ conanTheBarbarian.HoldTournament**.  

### how to play ###
Name Your Arena and create up to 8 fighters, if You like. Or watch 8 generic developer heroes fighting and dying in the ultimately deadly arena that is included in the game.  
#### the contesters and their kill-ability ####
Ada Lovelace, 8  
Donald Knuth, 5  
Konrad Zuse, 7  
Alan Turing, 8  
Margaret Hamilton, 10  
Edsger Dijkstra, 9  
Linus Torvalds, 7  
Richard Stallman, 5  
Ken Thompson, 6  
Niklaus Wirth, 6
### contact ###
Baldur Kellner - baldur.kellner@gmail.com